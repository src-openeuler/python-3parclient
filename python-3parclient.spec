%global _empty_manifest_terminate_build 0
Name:           python-3parclient
Version:        4.2.13
Release:        1
Summary:        HPE Alletra 9000 and HPE Primera and HPE 3PAR HTTP REST Client
License:        Apache-2.0
URL:            http://packages.python.org/python-3parclient
Source0:        https://files.pythonhosted.org/packages/ca/18/5b51a05f3cc615a00d26bfb71252a4237990170a00e57f0d0c92e29dfd91/python_3parclient-4.2.13.tar.gz
BuildArch:      noarch
%description
HPE 3PAR REST Client This is a Client library that can talk to the HPE 3PAR
Storage array. The 3PAR storage array has a REST web service interface and a
command line interface. This client library implements a simple interface for
talking with either interface, as needed. The python Requests library is used to
communicate with the REST interface.

%package -n python3-3parclient
Summary:        HPE Alletra 9000 and HPE Primera and HPE 3PAR HTTP REST Client
Provides:       python-3parclient
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
# General requires
BuildRequires:  python3-paramiko
BuildRequires:  python3-eventlet
BuildRequires:  python3-requests
BuildRequires:  python3-hatchling
# General requires
%description -n python3-3parclient
HPE 3PAR REST Client This is a Client library that can talk to the HPE 3PAR
Storage array. The 3PAR storage array has a REST web service interface and a
command line interface. This client library implements a simple interface for
talking with either interface, as needed. The python Requests library is used to
communicate with the REST interface.

%package help
Summary:        HPE Alletra 9000 and HPE Primera and HPE 3PAR HTTP REST Client
Provides:       python3-3parclient-doc
%description help
HPE 3PAR REST Client This is a Client library that can talk to the HPE 3PAR
Storage array. The 3PAR storage array has a REST web service interface and a
command line interface. This client library implements a simple interface for
talking with either interface, as needed. The python Requests library is used to
communicate with the REST interface.

%prep
%autosetup -n python_3parclient-%{version}

%build
%pyproject_build

%install
%pyproject_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
touch filelist.lst
if [ -d usr/lib64 ]; then
        find usr/lib64 -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
        find usr/bin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
        find usr/sbin -type f -printf "\"/%h/%f\"\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
        find usr/share/man -type f -printf "\"/%h/%f.gz\"\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-3parclient -f filelist.lst
%{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Wed Dec 11 2024 liutao1 <liutao1@kylinos.cn> - 4.2.13-1
- update package of version 4.2.13

* Mon Aug 28 2023 Ge Wang <wang__ge@126.com> - 4.2.12-3
- Generalize a common format for exclude file

* Mon Feb 27 2023 Ge Wang <wangge20@h-partners.com> - 4.2.12-2
- Remove redundency cache file

* Wed Jul 20 2022 renliang16 <renliang@uniontech.com> - 4.2.12-1
- Upgrade package python3-3parclient to version 4.2.12

* Tue Jul 20 2021 OpenStack_SIG <openstack@openeuler.org> - 4.2.11-1
- Package Spec generate
